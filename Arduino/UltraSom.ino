#include <Ultrasonic.h>
 
#define TRIGGER 4
#define ECHO 5
Ultrasonic ultrasonic(TRIGGER, ECHO);
float distancia = 0.0;
float sensor = 0.0;
float setPoint = 5.0;
//Configurações da Rede Perceptron Multicamadas.
  int i = 0, j = 0, k = 0, n = 0, funcao = 0;
  float soma = 0;

  int cx = 1;
  int c1 = 10;
  int c2 = 1;

  float w1[10] = { -2.341538, 2.341048, -2.341389, -2.341474, -2.341339, -2.341492, -2.341540, 2.341460, -2.341075, -2.341369};  
  float w2[10] = {-19.363735, 12.668363 , -17.427242, -18.506258, -16.798428, -18.796501, -19.399435, 18.182079, -13.269207, -17.141588}; 
  float dw1[10]; 
  float dw2[10];

  float auxC1[10];
  float auxC2[10];

  float entrada_camada1[10] = {0};
  float saida_camada1[10] = {0};
  float entrada_camada2[1] = {0};
  float saida_camada2[1] = {0};

  char saidas[1] = {0}; 
//Inicializa o sensor nos pinos definidos acima

float cmMsec = 0.0;
long microsec = 0;
  
void setup()
{
  pinMode(ECHO, INPUT);
  pinMode(TRIGGER, OUTPUT);
  pinMode(13, OUTPUT);    // Define pino 11 como saída (led vermelho)
 
      // Configuração do timer1 
  TCCR1A = 0;                        //confira timer para operação normal pinos OC1A e OC1B desconectados
  TCCR1B = 0;                        //limpa registrador
  TCCR1B |= (0 << CS10)|(0 << CS12); // configura prescaler para 8: CS12 = 0 e CS10 = 0
 
  TCNT1 = 0xFFFE;                    // incia timer com valor para que estouro ocorra em 0,000001 segundo
                                     // 65536-(16MHz/8/1Hz) = 65534 = 0xFFFE
  
  TIMSK1 |= (1 << TOIE1);           // habilita a interrupção do TIMER1
  Serial.begin(9600);
  Serial.println("Lendo dados do sensor...");
}
 
void loop()
{
  long microsec = ultrasonic.timing();
  cmMsec = ultrasonic.convert(microsec, Ultrasonic::CM);
  Serial.print("Distancia em cm: ");
  Serial.print(cmMsec);
  Serial.print("\r\n");

  sensor = setPoint/cmMsec;
  

       //Cálculo para camada C1.
        n = 0;
        for (j = 0; j < c1; j++){
            soma = 0;
            for (i = 0; i < cx; i++){
                soma += w1[n] * sensor;
                n += 1;
            }
            entrada_camada1[j] = soma;
            saida_camada1[j] = funcao_ativacao(entrada_camada1[j],funcao,1.0);
        }

        //Cálculo para camada C2.
        n = 0;
        for (j = 0; j < c2; j++){
        soma = 0;
            for (i = 0; i < c1; i++){
                soma += w2[n] * saida_camada1[i];
                n += 1;
        }
            entrada_camada2[j] = soma;
            saida_camada2[j] = funcao_ativacao(entrada_camada2[j],funcao,1.0);
            Serial.print("Saida da rede: ");
            Serial.print(saida_camada2[j], 6);
            Serial.print("\r\n");
          if (saida_camada2[0] < 0.5){
            digitalWrite(13, LOW);
          } else{
            digitalWrite(13, HIGH);
          }
        }

  delay(300);
}

float funcao_ativacao(float net, int funcao, float a){
    if (!funcao) return( 1.0 / (1.0 + exp(-a * net)) );
    return( (exp(a * net) - exp(-a * net)) / (exp(a * net) + exp(-a * net)) );
}


ISR(TIMER1_OVF_vect)                              //interrupção do TIMER1 
{
  contT++;
  Serial.print(contT);
  TCNT1 = 0xFFFE;                                 // Renicia TIMER
}