// ---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Vcl.ComCtrls.hpp>
#include "VaClasses.hpp"
#include "VaComm.hpp"
#include <FireDAC.VCLUI.Memo.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.SqlExpr.hpp>
#include <Data.DBXPool.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.DBXTrace.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Imaging.GIFImg.hpp>
#include "VaClasses.hpp"
#include <Vcl.Buttons.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.Provider.hpp>
#include <Vcl.Mask.hpp>
#include <Vcl.AppEvnts.hpp>

// ---------------------------------------------------------------------------
class TfrmPrincipal : public TForm {
__published: // IDE-managed Components

	TPageControl *gerenciadorDeAbas;
	TTabSheet *aba1;
	TTabSheet *aba2;
	TLabel *Label1;
	TLabel *Label2;
	TComboBox *portaSerial;
	TComboBox *baudRate;
	TButton *iniciarCom;
	TLabel *Label3;
	TEdit *neuroniosEntrada;
	TLabel *Label4;
	TEdit *neuroniosIntermediaria;
	TLabel *Label5;
	TEdit *saida;
	TLabel *Label6;
	TEdit *maximoEpocas;
	TLabel *Label7;
	TEdit *erroMinimo;
	TChart *graficoTreinamento;
	TButton *iniciarTrein;
	TVaComm *VaComm1;
	TTimer *Timer1;
	TLabel *Label8;
	TEdit *taxa_edit;
	TFDGUIxFormsMemo *Memo1;
	TFastLineSeries *Series2;
	TTeeGDIPlus *TeeGDIPlus1;
	TTeeGDIPlus *TeeGDIPlus2;
	TLabel *Label9;
	TEdit *resul_erro;
	TLabel *Label10;
	TEdit *resul_epocas;
	TRadioGroup *resul_tipo;
	TMemo *pesos_randomicos;
	TMemo *pesos_treinados;
	TSQLConnection *SQLConnection;
	TSQLQuery *SQLQuery1;
	TSQLDataSet *SQLDataSet2;
	TIntegerField *IntegerField1;
	TSQLDataSet *SQLDataSet1;
	TLabel *Label11;
	TEdit *momentum_edit;
	TComboBox *combo_rede;
	TLabel *Label13;
	TTabSheet *TabSheet1;
	TLabel *Label14;
	TComboBox *portaSerial2;
	TComboBox *portaSerial3;
	TLabel *Label15;
	TVaComm *VaComm2;
	TChart *graficoCar1_velocidade;
	TPanel *Panel1;
	TLabel *Label12;
	TPanel *Panel3;
	TLabel *Label19;
	TLabel *Label20;
	TLabel *Label21;
	TEdit *carrinho1_distancia;
	TEdit *carrinho1_velocidade;
	TPanel *Panel2;
	TLabel *Label16;
	TLabel *Label17;
	TLabel *Label18;
	TEdit *carrinho2_distancia;
	TEdit *carrinho2_velocidade;
	TPanel *Panel4;
	TLabel *Label22;
	TLabel *Label23;
	TLabel *Label24;
	TEdit *carrinho3_distancia;
	TEdit *carrinho3_velocidade;
	TPanel *Panel5;
	TLabel *Label25;
	TEdit *setpoint_velocidade;
	TButton *setpoint_enviar;
	TFastLineSeries *Series1;
	TChart *graficoCar1_distancia;
	TFastLineSeries *FastLineSeries1;
	TChart *graficoCar2_velocidade;
	TFastLineSeries *FastLineSeries2;
	TChart *graficoCar2_distancia;
	TFastLineSeries *FastLineSeries3;
	TChart *graficoCar3_velocidade;
	TFastLineSeries *FastLineSeries4;
	TChart *graficoCar3_distancia;
	TFastLineSeries *FastLineSeries5;
	TLabel *Label26;
	TIntegerField *SQLDataSet1CODIGO;
	TIntegerField *SQLDataSet1CARRINHO;
	TSingleField *SQLDataSet1DISTANCIA;
	TSingleField *SQLDataSet1VELOCIDADE;
	TTimer *Timer2;
	TComboBox *setpoint_carrinho;
	TVaComm *VaComm4;
	TDataSetProvider *dspConnection;
	TSQLDataSet *sqlDsConnection;
	TClientDataSet *cdsConsulta;
	TImage *Image3;
	TImage *Image2;
	TVaComm *VaComm3;
	TApplicationEvents *ApplicationEvents1;
	TImage *Image1;
	TImage *Image4;

	void __fastcall iniciarComClick(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall iniciarTreinClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall combo_redeChange(TObject *Sender);
	void __fastcall atualizaGrafico();
	void __fastcall Timer2Timer(TObject *Sender);
	void __fastcall setpoint_enviarClick(TObject *Sender);
	void __fastcall Comunica(TVaComm *c, int car);
	void __fastcall ApplicationEvents1Exception(TObject *Sender, Exception *E);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);


public: // User declarations

	__fastcall TfrmPrincipal(TComponent* Owner);

};

// ---------------------------------------------------------------------------
extern PACKAGE TfrmPrincipal *frmPrincipal;
// ---------------------------------------------------------------------------
#endif
